package plugin.eclat;

import java.io.File;
import java.io.IOException;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import plugin.eclat.EclatStepData;
import plugin.eclat.EclatStepMeta;

public class EclatStep extends BaseStep implements StepInterface 
{
	private static Class<?> PKG = EclatStepMeta.class;
	private EclatStepData data;
	private EclatStepMeta meta;
	
	public EclatStep(StepMeta s, StepDataInterface stepDataInterface, int c, TransMeta t, Trans dis) {
		super(s, stepDataInterface, c, t, dis);
	}
	
	public boolean ExecuteEclat() throws KettleException
	{
		//Let's check if Eclat initScript exists
		logBasic(BaseMessages.getString(PKG, "Eclat.Warning.LookingForExec"));
		File program = new File("/opt/Eclat/initScript");
		if(!program.exists())
		{
			logError(BaseMessages.getString(PKG, "Eclat.Error.CouldNotFindExecutable"));
			setErrors(1);
			return false;
		}
		
		// input file
		logBasic(BaseMessages.getString(PKG, "Eclat.Warning.LookingForInput"));
		File inputFile = new File(meta.getInputNameField());
		if(!inputFile.exists() || !inputFile.canRead())
		{
			logError(BaseMessages.getString(PKG, "Eclat.Error.CouldNotFindInputFile"));
			setErrors(1);
			return false;
		}
		
		// output permission check
		logBasic(BaseMessages.getString(PKG, "Eclat.Warning.CheckingForOutput"));
		File bln = new File(meta.getOutputNameField());
		try { bln.createNewFile(); }
		catch (IOException e) { e.printStackTrace(); }
		if(!bln.canWrite())
		{
	    	logError(BaseMessages.getString(PKG, "Eclat.Error.FailedOutputFile"));
	    	setErrors(1);
	    	return false;
		} else {
			bln.delete();
		}
		logBasic(" Ok!\n");
		
		
		// working directory (Eclat exec)
		File wd = new File("/opt/Eclat/");
		Process proc = null;
		
		// Let's try running it with the correct arguments
		try{
			logBasic(BaseMessages.getString(PKG, "Eclat.Warning.RunningEclat"));
			int trigger = 1;
			if(String.valueOf(meta.getAnticipationTriggerField()) == "false") trigger = 0;
			String arg = "./initScript" +
					" -t " + String.valueOf(trigger) +
					" -s " + String.valueOf(meta.getMinimumUserSupportField()) +
					" -c " + String.valueOf(meta.getMinimumUserConfidenceField()) +
					//" -l " + String.valueOf(meta.getMaximumLines()) +
					//" -ls " + String.valueOf(meta.getMaximumLineSize()) +
					" -i " + meta.getInputNameField() +
					" -o " + meta.getOutputNameField();
			proc = Runtime.getRuntime().exec(arg, null, wd);
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
			setErrors(1);
			return false;
		}

		return true;
	}

	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
		meta = (EclatStepMeta) smi;
		data = (EclatStepData) sdi;

		Object[] r = getRow(); // get row, blocks when needed!
		if (r == null) // no more input to be expected...
		{
			setOutputDone();
			return false;
		}

		if (first) {
			first = false;

			data.outputRowMeta = (RowMetaInterface) getInputRowMeta().clone();
			meta.getFields(data.outputRowMeta, getStepname(), null, null, this);

			logBasic(BaseMessages.getString(PKG, "Eclat.Warning.EclatStepInitialized"));
		}

		return true;
	}

	public boolean init(StepMetaInterface smi, StepDataInterface sdi)
	{
		meta = (EclatStepMeta) smi;
		data = (EclatStepData) sdi;
		
		try{
			ExecuteEclat();
		} catch (Exception e) {
			logError(BaseMessages.getString(PKG, "Eclat.Error.FailedExec") + " : " + e.toString());
		}

		return super.init(smi, sdi);
	}

	public void dispose(StepMetaInterface smi, StepDataInterface sdi)
	{
		meta = (EclatStepMeta) smi;
		data = (EclatStepData) sdi;

		super.dispose(smi, sdi);
	}

	//
	// Run is were the action happens!
	public void run()
	{
		try {
			while (processRow(meta, data) && !isStopped())
				;
		} catch (Exception e) {
			logError("Unexpected error : " + e.toString());
			logError(Const.getStackTracker(e));
			setErrors(1);
			stopAll();
		} finally {
			dispose(meta, data);
			logBasic("Finished, processing " + getLinesRead() + " rows");
			markStop();
		}
	}
}
