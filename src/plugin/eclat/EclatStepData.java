package plugin.eclat;

import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.StepDataInterface;

public class EclatStepData  extends BaseStepData implements StepDataInterface
{
	public RowMetaInterface outputRowMeta;
	
	public EclatStepData()
	{
		super();
	}
}
