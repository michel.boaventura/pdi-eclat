package plugin.eclat;

import javax.swing.JFileChooser;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
//import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Scale;
import org.pentaho.di.core.Const;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.ui.trans.step.BaseStepDialog;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDialogInterface;
import plugin.eclat.EclatStepMeta;


public class EclatStepDialog extends BaseStepDialog implements StepDialogInterface
{
	private static Class<?> PKG = EclatStepMeta.class;
	private EclatStepMeta input;
	
	private Label wlAnticipationName, wlMinimumUserSupportName, wlMinimumUserConfidenceName,
					wlMinimumUserSupportValue, wlMinimumUserConfidenceValue,
					//wlMaxLines, wlMaxLineSize,
					wlInputName, wlOutputName;
	private Text wInputName, wOutputName;
	private FormData fdlAnticipationName, fdAnticipationName,
					 fdlMinimumUserSupportName, fdMinimumUserSupportName,
					 fdlMinimumUserConfidenceName, fdMinimumUserConfidenceName,
					 //fdlMaxLines, fdMaxLines,
					 //fdlMaxLineSize, fdMaxLineSize,
					 fdlInputName, fdInputName,
					 fdlOutputName, fdOutputName,
					 fdbInputName, fdbOutputName;
	private Button wbInputName, wbOutputName, wbAnticipationName;
	private JFileChooser fci, fco;
	private Listener lsChooseInput, lsChooseOutput;
	private Scale sMinimumUserSupport, sMinimumUserConfidence;
	//private Spinner sMaxLineSize, sMaxLines;
	
	private int spacing = 15;
	
	public EclatStepDialog(Shell parent, Object in, TransMeta transMeta, String sname)
	{
		super(parent, (BaseStepMeta) in, transMeta, sname);
		input = (EclatStepMeta) in;
	}
	
	public String open()
	{
		Shell parent = getParent();
		Display display = parent.getDisplay();

		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MIN | SWT.MAX);
		props.setLook(shell);
		setShellImage(shell, input);

		ModifyListener lsMod = new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				input.setChanged();
			}
		};
		
		changed = input.hasChanged();

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;

		shell.setLayout(formLayout);
		shell.setText(BaseMessages.getString(PKG, "Eclat step")); 

		int middle = props.getMiddlePct();
		int margin = Const.MARGIN;

		// Step name
		wlStepname = new Label(shell, SWT.RIGHT);
		wlStepname.setText(BaseMessages.getString(PKG, "System.Label.StepName")); 
		props.setLook(wlStepname);
		fdlStepname = new FormData();
		fdlStepname.left = new FormAttachment(0, 0);
		fdlStepname.right = new FormAttachment(middle, -margin);
		fdlStepname.top = new FormAttachment(0, margin);
		wlStepname.setLayoutData(fdlStepname);
		
		wStepname = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wStepname.setText(stepname);
		props.setLook(wStepname);
		wStepname.addModifyListener(lsMod);
		fdStepname = new FormData();
		fdStepname.left = new FormAttachment(middle, 0);
		fdStepname.right = new FormAttachment(100, 0);
		fdStepname.top = new FormAttachment(0, margin);
		wStepname.setLayoutData(fdStepname);
		
		// Anticipation name
		wlAnticipationName = new Label(shell, SWT.RIGHT);
		wlAnticipationName.setText(BaseMessages.getString(PKG, "Eclat.AnticipationName.Label")); 
		props.setLook(wlAnticipationName);
		fdlAnticipationName = new FormData();
		fdlAnticipationName.left = new FormAttachment(0, 0);
		fdlAnticipationName.right = new FormAttachment(middle, -margin);
		fdlAnticipationName.top = new FormAttachment(wStepname, margin + spacing);
		wlAnticipationName.setLayoutData(fdlAnticipationName);
		
		wbAnticipationName = new Button(shell, SWT.CHECK);
		props.setLook(wbAnticipationName);
		fdAnticipationName = new FormData();
		fdAnticipationName.left = new FormAttachment(middle, 0);
		fdAnticipationName.top = new FormAttachment(wStepname, margin + spacing);
		wbAnticipationName.setLayoutData(fdAnticipationName);
		
		// Minimum user support name
		wlMinimumUserSupportName = new Label(shell, SWT.RIGHT);
		wlMinimumUserSupportName.setText(BaseMessages.getString(PKG, "Eclat.MinimumUserSupportName.Label")); 
		props.setLook(wlMinimumUserSupportName);
		fdlMinimumUserSupportName = new FormData();
		fdlMinimumUserSupportName.left = new FormAttachment(0, 0);
		fdlMinimumUserSupportName.right = new FormAttachment(middle, -margin);
		fdlMinimumUserSupportName.top = new FormAttachment(wlAnticipationName, margin + spacing);
		wlMinimumUserSupportName.setLayoutData(fdlMinimumUserSupportName);
		
		wlMinimumUserSupportValue = new Label(shell, SWT.RIGHT);
		wlMinimumUserSupportValue.setText("100");
		props.setLook(wlMinimumUserSupportValue);
		fdMinimumUserSupportName = new FormData();
		fdMinimumUserSupportName.left = new FormAttachment(middle, 0);
		fdMinimumUserSupportName.top = new FormAttachment(wlAnticipationName, margin + spacing);
		wlMinimumUserSupportValue.setLayoutData(fdMinimumUserSupportName);
		
		sMinimumUserSupport = new Scale(shell, SWT.HORIZONTAL);
		props.setLook(sMinimumUserSupport);
		fdMinimumUserSupportName = new FormData();
		fdMinimumUserSupportName.left = new FormAttachment(wlMinimumUserSupportValue, margin);
		fdMinimumUserSupportName.right = new FormAttachment(100, 0);
		fdMinimumUserSupportName.top = new FormAttachment(wlAnticipationName, margin + spacing);
		sMinimumUserSupport.setLayoutData(fdMinimumUserSupportName);
		sMinimumUserSupport.setMaximum(100);
		sMinimumUserSupport.setMinimum(0);
		sMinimumUserSupport.setIncrement(1);
		sMinimumUserSupport.setPageIncrement(1);
		sMinimumUserSupport.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				wlMinimumUserSupportValue.setText(String.valueOf(sMinimumUserSupport.getSelection()));
			}
		});
		
		// Minimum user confidence
		wlMinimumUserConfidenceName = new Label(shell, SWT.RIGHT);
		wlMinimumUserConfidenceName.setText(BaseMessages.getString(PKG, "Eclat.MinimumUserConfidenceName.Label")); 
		props.setLook(wlMinimumUserConfidenceName);
		fdlMinimumUserConfidenceName = new FormData();
		fdlMinimumUserConfidenceName.left = new FormAttachment(0, 0);
		fdlMinimumUserConfidenceName.right = new FormAttachment(middle, -margin);
		fdlMinimumUserConfidenceName.top = new FormAttachment(wlMinimumUserSupportName, margin + spacing);
		wlMinimumUserConfidenceName.setLayoutData(fdlMinimumUserConfidenceName);
		
		wlMinimumUserConfidenceValue = new Label(shell, SWT.RIGHT);
		wlMinimumUserConfidenceValue.setText("100");
		props.setLook(wlMinimumUserConfidenceValue);
		fdMinimumUserConfidenceName = new FormData();
		fdMinimumUserConfidenceName.left = new FormAttachment(middle, 0);
		fdMinimumUserConfidenceName.top = new FormAttachment(wlMinimumUserSupportName, margin + spacing);
		wlMinimumUserConfidenceValue.setLayoutData(fdMinimumUserConfidenceName);
					
		sMinimumUserConfidence = new Scale(shell, SWT.HORIZONTAL);
		props.setLook(sMinimumUserSupport);
		fdMinimumUserConfidenceName = new FormData();
		fdMinimumUserConfidenceName.left = new FormAttachment(wlMinimumUserConfidenceValue, margin);
		fdMinimumUserConfidenceName.right = new FormAttachment(100, 0);
		fdMinimumUserConfidenceName.top = new FormAttachment(wlMinimumUserSupportName, margin + spacing);
		sMinimumUserConfidence.setLayoutData(fdMinimumUserConfidenceName);
		sMinimumUserConfidence.setMaximum(100);
		sMinimumUserConfidence.setMinimum(0);
		sMinimumUserConfidence.setIncrement(1);
		sMinimumUserConfidence.setPageIncrement(1);
		sMinimumUserConfidence.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				wlMinimumUserConfidenceValue.setText(String.valueOf(sMinimumUserConfidence.getSelection()));
			}
		});
		
		/*
		// Maxlines
		wlMaxLines = new Label(shell, SWT.RIGHT);
		wlMaxLines.setText(BaseMessages.getString(PKG, "Eclat.MaximumLines.label")); 
		props.setLook(wlMaxLines);
		fdlMaxLines = new FormData();
		fdlMaxLines.left = new FormAttachment(0, 0);
		fdlMaxLines.right = new FormAttachment(middle, -margin);
		fdlMaxLines.top = new FormAttachment(wlMinimumUserConfidenceName, margin + spacing);
		wlMaxLines.setLayoutData(fdlMaxLines);
		
		sMaxLines = new Spinner(shell, SWT.NONE);
		sMaxLines.setMinimum(1);
		sMaxLines.setMaximum(400000000);
		fdMaxLines = new FormData();
		fdMaxLines.left = new FormAttachment(middle, 0);
		fdMaxLines.right = new FormAttachment(100, 0);
		fdMaxLines.top = new FormAttachment(wlMinimumUserConfidenceName, margin + spacing);
		sMaxLines.setLayoutData(fdMaxLines);
		
		// Max line size
		wlMaxLineSize = new Label(shell, SWT.RIGHT);
		wlMaxLineSize.setText(BaseMessages.getString(PKG, "Eclat.MaximumLineSize.label")); 
		props.setLook(wlMaxLineSize);
		fdlMaxLineSize = new FormData();
		fdlMaxLineSize.left = new FormAttachment(0, 0);
		fdlMaxLineSize.right = new FormAttachment(middle, -margin);
		fdlMaxLineSize.top = new FormAttachment(wlMaxLines, margin + spacing);
		wlMaxLineSize.setLayoutData(fdlMaxLineSize);
				
		sMaxLineSize = new Spinner(shell, SWT.NONE);
		sMaxLineSize.setMinimum(1);
		sMaxLineSize.setMaximum(400000000);
		fdMaxLineSize = new FormData();
		fdMaxLineSize.left = new FormAttachment(middle, 0);
		fdMaxLineSize.right = new FormAttachment(100, 0);
		fdMaxLineSize.top = new FormAttachment(wlMaxLines, margin + spacing);
		sMaxLineSize.setLayoutData(fdMaxLineSize);*/
				
		// Input file name
		wlInputName = new Label(shell, SWT.RIGHT);
		wlInputName.setText(BaseMessages.getString(PKG, "Eclat.InputName.Label")); 
		props.setLook(wlInputName);
		fdlInputName = new FormData();
		fdlInputName.left = new FormAttachment(0, 0);
		fdlInputName.right = new FormAttachment(middle, -margin);
		//fdlInputName.top = new FormAttachment(wlMaxLineSize, margin + spacing);
		fdlInputName.top = new FormAttachment(wlMinimumUserConfidenceName, margin + spacing);
		wlInputName.setLayoutData(fdlInputName);
						
		wInputName = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(wInputName);
		wInputName.addModifyListener(lsMod);
		fdInputName = new FormData();
		fdInputName.left = new FormAttachment(middle, 0);
		fdInputName.right = new FormAttachment(100, -75);
		//fdInputName.top = new FormAttachment(wlMaxLineSize, margin + spacing);
		fdInputName.top = new FormAttachment(wlMinimumUserConfidenceName, margin + spacing);
		wInputName.setLayoutData(fdInputName);
		
		// Input browse button
		wbInputName = new Button(shell, SWT.PUSH | SWT.CENTER);
		props.setLook(wbInputName);
		wbInputName.setText(BaseMessages.getString(PKG, "Eclat.BrowseInputName.Label"));
		fdbInputName = new FormData();
		fdbInputName.left = new FormAttachment(wInputName, margin);
		//fdbInputName.top = new FormAttachment(wlMaxLineSize, margin + spacing);
		fdbInputName.top = new FormAttachment(wlMinimumUserConfidenceName, margin + spacing);
		wbInputName.setLayoutData(fdbInputName);
		lsChooseInput = new Listener()
		{
			public void handleEvent(Event e)
			{
				try {
					fci.showOpenDialog(null);
					if(fci.getSelectedFile().toString() != null)
						wInputName.setText(fci.getSelectedFile().toString());
				} catch (Exception x){
					// do nothing, no file selected
				}
			}
		};
		wbInputName.addListener(SWT.Selection, lsChooseInput);
		
		// Output file name
		wlOutputName = new Label(shell, SWT.RIGHT);
		wlOutputName.setText(BaseMessages.getString(PKG, "Eclat.OutputName.Label")); 
		props.setLook(wlOutputName);
		fdlOutputName = new FormData();
		fdlOutputName.left = new FormAttachment(0, 0);
		fdlOutputName.right = new FormAttachment(middle, -margin);
		fdlOutputName.top = new FormAttachment(wlInputName, margin + spacing);
		wlOutputName.setLayoutData(fdlOutputName);
						
		wOutputName = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(wOutputName);
		wOutputName.addModifyListener(lsMod);
		fdOutputName = new FormData();
		fdOutputName.left = new FormAttachment(middle, 0);
		fdOutputName.right = new FormAttachment(100, -75);
		fdOutputName.top = new FormAttachment(wlInputName, margin + spacing);
		wOutputName.setLayoutData(fdOutputName);
		
		// Output browse button
		wbOutputName = new Button(shell, SWT.PUSH | SWT.CENTER);
		props.setLook(wbOutputName);
		wbOutputName.setText(BaseMessages.getString(PKG, "Eclat.BrowseOutputName.Label"));
		fdbOutputName = new FormData();
		fdbOutputName.left = new FormAttachment(wOutputName, margin);
		fdbOutputName.top = new FormAttachment(wlInputName, margin + spacing);
		wbOutputName.setLayoutData(fdbOutputName);
		lsChooseOutput = new Listener()
		{
			public void handleEvent(Event e)
			{
				try {
					fco.showSaveDialog(null);
					if(fco.getSelectedFile().toString() != null)
							wOutputName.setText(fco.getSelectedFile().toString());
				} catch (Exception x){
					// do nothing, no file selected
				}
			}
		};
		wbOutputName.addListener(SWT.Selection, lsChooseOutput);

		// OK and cancel buttons
		wOK = new Button(shell, SWT.PUSH);
		wOK.setText(BaseMessages.getString(PKG, "System.Button.OK")); 
		wCancel = new Button(shell, SWT.PUSH);
		wCancel.setText(BaseMessages.getString(PKG, "System.Button.Cancel")); 

		BaseStepDialog.positionBottomButtons(shell, new Button[] { wOK, wCancel }, margin, wlOutputName);
		
		// Add listeners
		lsCancel = new Listener()
		{
			public void handleEvent(Event e)
			{
				cancel();
			}
		};
		lsOK = new Listener()
		{
			public void handleEvent(Event e)
			{
				ok();
			}
		};

		wCancel.addListener(SWT.Selection, lsCancel);
		wOK.addListener(SWT.Selection, lsOK);

		lsDef = new SelectionAdapter()
		{
			public void widgetDefaultSelected(SelectionEvent e) 
			{
				ok();
			}
		};

		wbAnticipationName.addSelectionListener(lsDef);
		wStepname.addSelectionListener(lsDef);
		wInputName.addSelectionListener(lsDef);
		wOutputName.addSelectionListener(lsDef);
		

		// Detect X or ALT-F4 or something that kills this window...
		shell.addShellListener(new ShellAdapter()
		{
			public void shellClosed(ShellEvent e)
			{
				cancel();
			}
		});

			
		// Set the shell size, based upon previous time...
		setSize();

		getData();
		input.setChanged(changed);

		shell.open();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())	display.sleep();
		}
		return stepname;
	}
	
	// Read data and place it in the dialog
	public void getData()
	{
		wStepname.selectAll();
		wbAnticipationName.setSelection(input.getAnticipationTriggerField());
		
		sMinimumUserSupport.setSelection(input.getMinimumUserSupportField());
		wlMinimumUserSupportValue.setText(String.valueOf(sMinimumUserSupport.getSelection()));
		sMinimumUserConfidence.setSelection(input.getMinimumUserConfidenceField());
		wlMinimumUserConfidenceValue.setText(String.valueOf(sMinimumUserConfidence.getSelection()));
		
		//sMaxLines.setSelection(input.getMaximumLines());
		//sMaxLineSize.setSelection(input.getMaximumLineSize());
		
		wInputName.setText(input.getInputNameField());
		wOutputName.setText(input.getOutputNameField());
		
		fci = new JFileChooser(wInputName.getText());
		fco = new JFileChooser(wOutputName.getText());
	}

	private void cancel()
	{
		stepname = null;
		input.setChanged(changed);
		
		dispose();
	}
		
	// let the plugin know about the entered data
	private void ok()
	{
		stepname = wStepname.getText();
		input.setAnticipationTriggerField(wbAnticipationName.getSelection());
		
		input.setMinimumUserSupportField(sMinimumUserSupport.getSelection());
		input.setMinimumUserConfidenceField(sMinimumUserConfidence.getSelection());
		
		//input.setMaximumLines(sMaxLines.getSelection());
		//input.setMaximumLineSize(sMaxLineSize.getSelection());
		
		input.setInputNameField(wInputName.getText());
		input.setOutputNameField(wOutputName.getText());
		
		dispose();
	}
}
