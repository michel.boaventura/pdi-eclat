package plugin.eclat;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;
import org.pentaho.di.core.CheckResult;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Counter;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.row.RowMetaInterface;
//import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.w3c.dom.Node;

import plugin.eclat.EclatStep;
import plugin.eclat.EclatStepData;
import plugin.eclat.EclatStepDialog;
import plugin.eclat.EclatStepMeta;

public class EclatStepMeta extends BaseStepMeta implements StepMetaInterface {
	private static Class<?> PKG = EclatStepMeta.class; // for i18n purposes
	private String InputName, OutputName;
	private int MinimumUserSupport, MinimumUserConfidence;
				//MaximumLines, MaximumLineSize;
	private boolean AnticipationTrigger;

	public EclatStepMeta() {
		super(); 
	}

	public boolean getAnticipationTriggerField()
	{
		return AnticipationTrigger;
	}

	public void setAnticipationTriggerField(boolean outputField)
	{
		this.AnticipationTrigger = outputField;
	}
	
	public int getMinimumUserSupportField()
	{
		return MinimumUserSupport;
	}

	public void setMinimumUserSupportField(int outputField)
	{
		this.MinimumUserSupport = outputField;
	}
	
	public int getMinimumUserConfidenceField()
	{
		return MinimumUserConfidence;
	}

	public void setMinimumUserConfidenceField(int outputField)
	{
		this.MinimumUserConfidence = outputField;
	}
	
	/*public void setMaximumLines(int outputField)
	{
		this.MaximumLines = outputField; 
	}
	
	public int getMaximumLines()
	{
		return MaximumLines; 
	}
	
	public void setMaximumLineSize(int outputField)
	{
		this.MaximumLineSize = outputField; 
	}
	
	public int getMaximumLineSize()
	{
		return MaximumLineSize; 
	}*/
	
	public String getInputNameField()
	{
		return InputName;
	}
	
	public void setInputNameField(String outputField)
	{
		this.InputName = outputField;
	}
	
	public String getOutputNameField()
	{
		return OutputName;
	}
	
	public void setOutputNameField(String outputField)
	{
		this.OutputName = outputField;
	}

	public String getXML() throws KettleValueException
	{
		String retval = "";
		retval += "		<anticipationtriggerfield>" + getAnticipationTriggerField() + "</anticipationtriggerfield>" + Const.CR;
		retval += "		<minimumusersupportfield>" + getMinimumUserSupportField() + "</minimumusersupportfield>" + Const.CR;
		retval += "		<minimumuserconfidencefield>" + getMinimumUserConfidenceField() + "</minimumuserconfidencefield>" + Const.CR;
		//retval += "		<maximumlines>" + getMaximumLines() + "</maximumlines>" + Const.CR;
		//retval += "		<maximumlinesize>" + getMaximumLineSize() + "</maximumlinesize>" + Const.CR;
		retval += "		<inputnamefield>" + getInputNameField() + "</inputnamefield>" + Const.CR;
		retval += "		<outputnamefield>" + getOutputNameField() + "</outputnamefield>" + Const.CR;
		
		return retval;
	}

	public Object clone()
	{
		Object retval = super.clone();
		return retval;
	}

	public void loadXML(Node stepnode, List<DatabaseMeta> databases, Map<String, Counter> counters) throws KettleXMLException
	{
		try
		{
			setAnticipationTriggerField(Boolean.parseBoolean(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "anticipationtriggerfield"))));
			setMinimumUserSupportField(Integer.parseInt(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "minimumusersupportfield"))));
			setMinimumUserConfidenceField(Integer.parseInt(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "minimumuserconfidencefield"))));
			//setMaximumLines(Integer.parseInt(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "maximumlines"))));
			//setMaximumLineSize(Integer.parseInt(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "maximumlinesize"))));
			setInputNameField(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "inputnamefield")));
			setOutputNameField(XMLHandler.getNodeValue(XMLHandler.getSubNode(stepnode, "outputnamefield")));
			if(InputName == null) InputName = "";
			if(OutputName == null) OutputName = "";
		} catch (Exception e) {
			throw new KettleXMLException("Eclat Plugin Unable to read step info from XML node", e);
		}
	}

	public void setDefault()
	{
		AnticipationTrigger = false;
		MinimumUserSupport = 50;
		MinimumUserConfidence = 50;
		//MaximumLines = 5000;
		//MaximumLineSize = 1000;
		InputName = "";
		OutputName = "";
	}

	public void check(List<CheckResultInterface> remarks, TransMeta transmeta, StepMeta stepMeta, RowMetaInterface prev, String input[], String output[], RowMetaInterface info) {
		CheckResult cr;

		// See if we have input streams leading to this step!
		if (input.length > 0)
		{
			cr = new CheckResult(CheckResult.TYPE_RESULT_OK, "Step is receiving info from other steps.", stepMeta);
			remarks.add(cr);
		} else {
			cr = new CheckResult(CheckResult.TYPE_RESULT_ERROR, "No input received from other steps!", stepMeta);
			remarks.add(cr);
		}	
    	
	}

	public StepDialogInterface getDialog(Shell shell, StepMetaInterface meta, TransMeta transMeta, String name)
	{
		return new EclatStepDialog(shell, meta, transMeta, name);
	}
	
	public StepInterface getStep(StepMeta stepMeta, StepDataInterface stepDataInterface, int cnr, TransMeta transMeta, Trans disp)
	{
		return new EclatStep(stepMeta, stepDataInterface, cnr, transMeta, disp);
	}

	public StepDataInterface getStepData()
	{
		return new EclatStepData();
	}

	public void readRep(Repository rep, ObjectId id_step, List<DatabaseMeta> databases, Map<String, Counter> counters) throws KettleException
	{
		try
		{
			AnticipationTrigger = Boolean.parseBoolean(rep.getStepAttributeString(id_step, "anticipationtriggerfield"));
			MinimumUserSupport = (int) rep.getStepAttributeInteger(id_step, "minimumusersupportfield");
			MinimumUserConfidence = (int) rep.getStepAttributeInteger(id_step, "minimumuserconfidencefield");
			//MaximumLines = (int) rep.getStepAttributeInteger(id_step, "maximumlines");
			//MaximumLineSize = (int) rep.getStepAttributeInteger(id_step, "maximumlinesize");
			InputName = rep.getStepAttributeString(id_step, "inputnamefield");
			OutputName = rep.getStepAttributeString(id_step, "outputnamefield");
			
		}
		catch(Exception e)
		{
			throw new KettleException(BaseMessages.getString(PKG, "EclatStep.Exception.UnexpectedErrorInReadingStepInfo"), e);
		}
	}

	public void saveRep(Repository rep, ObjectId id_transformation, ObjectId id_step) throws KettleException
	{
		try
		{
			rep.saveStepAttribute(id_transformation, id_step, "anticipationtriggerfield", AnticipationTrigger);
			rep.saveStepAttribute(id_transformation, id_step, "minimumusersupportfield", MinimumUserSupport);
			rep.saveStepAttribute(id_transformation, id_step, "minimumuserconfidencefield", MinimumUserConfidence);
			//rep.saveStepAttribute(id_transformation, id_step, "maximumlines", MaximumLines);
			//rep.saveStepAttribute(id_transformation, id_step, "maximumlinesize", MaximumLineSize);
			rep.saveStepAttribute(id_transformation, id_step, "inputnamefield", InputName);
			rep.saveStepAttribute(id_transformation, id_step, "outputnamefield", OutputName);
		}
		catch(Exception e)
		{
			throw new KettleException(BaseMessages.getString(PKG, "EclatStep.Exception.UnableToSaveStepInfoToRepository")+id_step, e); 
		}
	}
}
